# For POCO M5
echo `adb shell pm uninstall com.miui.daemon`
echo `adb shell pm uninstall --user 0 com.miui.daemon`
echo `adb shell pm uninstall com.miui.miservice`
echo `adb shell pm uninstall --user 0 com.miui.miservice`
echo `adb shell pm uninstall com.mi.android.globalFileexplorer`
echo `adb shell pm uninstall --user 0 com.mi.android.globalFileexplorer`
echo `adb shell pm uninstall com.miui.msa.global`
echo `adb shell pm uninstall --user 0 com.miui.msa.global`
echo `adb shell pm uninstall com.linkedin.android`
echo `adb shell pm uninstall --user 0 com.linkedin.android`
echo `adb shell pm uninstall com.mi.android.globalminusscreen`
echo `adb shell pm uninstall --user 0 com.mi.android.globalminusscreen`
#echo `adb shell pm uninstall com.google.android.apps.nbu.files`
#echo `adb shell pm uninstall --user 0 com.google.android.apps.nbu.files`
echo `adb shell pm uninstall com.miui.fm`
echo `adb shell pm uninstall --user 0 com.miui.fm`
#echo `adb shell pm uninstall com.huaqin.sarcontroller`
#echo `adb shell pm uninstall --user 0 com.huaqin.sarcontroller`
echo `adb shell pm uninstall com.facebook.appmanager`
echo `adb shell pm uninstall --user 0 com.facebook.appmanager`
echo `adb shell pm uninstall com.xiaomi.xmsf`
echo `adb shell pm uninstall --user 0 com.xiaomi.xmsf`
echo `adb shell pm uninstall in.amazon.mShop.android.shopping`
echo `adb shell pm uninstall --user 0 in.amazon.mShop.android.shopping`
echo `adb shell pm uninstall com.xiaomi.micloud.sdk`
echo `adb shell pm uninstall --user 0 com.xiaomi.micloud.sdk`
echo `adb shell pm uninstall com.miui.phone.carriers.overlay.vodafone`
echo `adb shell pm uninstall --user 0 com.miui.phone.carriers.overlay.vodafone`
#echo `adb shell pm uninstall com.miui.global.packageinstaller`
#echo `adb shell pm uninstall --user 0 com.miui.global.packageinstaller`
echo `adb shell pm uninstall com.miui.micloudsync`
echo `adb shell pm uninstall --user 0 com.miui.micloudsync`
echo `adb shell pm uninstall com.google.android.adservices.api`
echo `adb shell pm uninstall --user 0 com.google.android.adservices.api`
#not uninstallable through adb, requires manual intervention 
echo `adb shell pm uninstall com.xiaomi.scanner`
echo `adb shell pm uninstall --user 0 com.xiaomi.scanner`
echo `adb shell pm uninstall com.google.android.ondevicepersonalization.services`
echo `adb shell pm uninstall --user 0 com.google.android.ondevicepersonalization.services`
#not uninstallable through adb, requires manual intervention 
echo `adb shell pm uninstall com.miui.weather2`
echo `adb shell pm uninstall --user 0 com.miui.weather2`
echo `adb shell pm uninstall com.miui.analytics`
echo `adb shell pm uninstall --user 0 com.miui.analytics`
echo `adb shell pm uninstall com.xiaomi.joyose`
echo `adb shell pm uninstall --user 0 com.xiaomi.joyose`
#echo `adb shell pm uninstall com.miui.audiomonitor`
#echo `adb shell pm uninstall --user 0 com.miui.audiomonitor`
echo `adb shell pm uninstall com.xiaomi.account`
echo `adb shell pm uninstall --user 0 com.xiaomi.account`
echo `adb shell pm uninstall com.amazon.avod.thirdpartyclient`
echo `adb shell pm uninstall --user 0 com.amazon.avod.thirdpartyclient`
echo `adb shell pm uninstall com.google.android.apps.googleassistant`
echo `adb shell pm uninstall --user 0 com.google.android.apps.googleassistant`
#echo `adb shell pm uninstall com.huaqin.diaglogger`
#echo `adb shell pm uninstall --user 0 com.huaqin.diaglogger`
echo `adb shell pm uninstall com.xiaomi.payment`
echo `adb shell pm uninstall --user 0 com.xiaomi.payment`
echo `adb shell pm uninstall com.google.android.apps.tachyon`
echo `adb shell pm uninstall --user 0 com.google.android.apps.tachyon`
echo `adb shell pm uninstall com.google.android.apps.magazines`
echo `adb shell pm uninstall --user 0 com.google.android.apps.magazines`
echo `adb shell pm uninstall com.xiaomi.simactivate.service`
echo `adb shell pm uninstall --user 0 com.xiaomi.simactivate.service`
echo `adb shell pm uninstall com.spotify.music`
echo `adb shell pm uninstall --user 0 com.spotify.music`
#echo `adb shell pm uninstall com.huaqin.clearefs`
#echo `adb shell pm uninstall --user 0 com.huaqin.clearefs`
echo `adb shell pm uninstall com.miui.cloudbackup`
echo `adb shell pm uninstall --user 0 com.miui.cloudbackup`
echo `adb shell pm uninstall com.google.android.dialer`
echo `adb shell pm uninstall --user 0 com.google.android.dialer`
echo `adb shell pm uninstall com.xiaomi.glgm`
echo `adb shell pm uninstall --user 0 com.xiaomi.glgm`
#not uninstallable through adb, requires manual intervention 
echo `adb shell pm uninstall com.android.soundrecorder`
echo `adb shell pm uninstall --user 0 com.android.soundrecorder`
echo `adb shell pm uninstall com.netflix.mediaclient`
echo `adb shell pm uninstall --user 0 com.netflix.mediaclient`
echo `adb shell pm uninstall com.google.mainline.adservices`
echo `adb shell pm uninstall --user 0 com.google.mainline.adservices`
#not uninstallable through adb, requires manual intervention 
echo `adb shell pm uninstall com.xiaomi.midrop`
echo `adb shell pm uninstall --user 0 com.xiaomi.midrop`
echo `adb shell pm uninstall com.android.hotwordenrollment.okgoogle`
echo `adb shell pm uninstall --user 0 com.android.hotwordenrollment.okgoogle`
echo `adb shell pm uninstall com.miui.player`
echo `adb shell pm uninstall --user 0 com.miui.player`
#echo `adb shell pm uninstall com.google.android.overlay.gmsconfig.personalsafety`
#echo `adb shell pm uninstall --user 0 com.google.android.overlay.gmsconfig.personalsafety`
echo `adb shell pm uninstall com.duokan.phone.remotecontroller`
echo `adb shell pm uninstall --user 0 com.duokan.phone.remotecontroller`
#not uninstallable through adb, requires manual intervention 
echo `adb shell pm uninstall com.miui.compass`
echo `adb shell pm uninstall --user 0 com.miui.compass`
echo `adb shell pm uninstall com.mintgames.zentriple3d`
echo `adb shell pm uninstall --user 0 com.mintgames.zentriple3d`
echo `adb shell pm uninstall com.android.hotwordenrollment.xgoogle`
echo `adb shell pm uninstall --user 0 com.android.hotwordenrollment.xgoogle`
echo `adb shell pm uninstall com.snapchat.android`
echo `adb shell pm uninstall --user 0 com.snapchat.android`
echo `adb shell pm uninstall com.google.android.apps.youtube.music`
echo `adb shell pm uninstall --user 0 com.google.android.apps.youtube.music`
echo `adb shell pm uninstall com.google.android.partnersetup`
echo `adb shell pm uninstall --user 0 com.google.android.partnersetup`
echo `adb shell pm uninstall com.google.android.projection.gearhead`
echo `adb shell pm uninstall --user 0 com.google.android.projection.gearhead`
#not uninstallable through adb, requires manual intervention 
echo `adb shell pm uninstall com.miui.notes`
echo `adb shell pm uninstall --user 0 com.miui.notes`
echo `adb shell pm uninstall com.google.android.videos`
echo `adb shell pm uninstall --user 0 com.google.android.videos`
echo `adb shell pm uninstall com.xiaomi.finddevice`
echo `adb shell pm uninstall --user 0 com.xiaomi.finddevice`
echo `adb shell pm uninstall com.google.android.feedback`
echo `adb shell pm uninstall --user 0 com.google.android.feedback`
echo `adb shell pm uninstall com.xiaomi.xmsfkeeper`
echo `adb shell pm uninstall --user 0 com.xiaomi.xmsfkeeper`
echo `adb shell pm uninstall com.android.chrome`
echo `adb shell pm uninstall --user 0 com.android.chrome`
echo `adb shell pm uninstall android.autoinstalls.config.Xiaomi.model`
echo `adb shell pm uninstall --user 0 android.autoinstalls.config.Xiaomi.model`
echo `adb shell pm uninstall com.facebook.services`
echo `adb shell pm uninstall --user 0 com.facebook.services`
echo `adb shell pm uninstall com.google.android.apps.nbu.paisa.user`
echo `adb shell pm uninstall --user 0 com.google.android.apps.nbu.paisa.user`
echo `adb shell pm uninstall com.preff.kb.xm`
echo `adb shell pm uninstall --user 0 com.preff.kb.xm`
echo `adb shell pm uninstall com.google.android.apps.chromecast.app`
echo `adb shell pm uninstall --user 0 com.google.android.apps.chromecast.app`
echo `adb shell pm uninstall com.mintgames.schuman.ablock`
echo `adb shell pm uninstall --user 0 com.mintgames.schuman.ablock`
echo `adb shell pm uninstall com.google.android.inputmethod.latin`
echo `adb shell pm uninstall --user 0 com.google.android.inputmethod.latin`
echo `adb shell pm uninstall com.miui.bugreport`
echo `adb shell pm uninstall --user 0 com.miui.bugreport`
echo `adb shell pm uninstall com.google.android.marvin.talkback`
echo `adb shell pm uninstall --user 0 com.google.android.marvin.talkback`
echo `adb shell pm uninstall com.miui.yellowpage`
echo `adb shell pm uninstall --user 0 com.miui.yellowpage`
echo `adb shell pm uninstall cn.wps.xiaomi.abroad.lite`
echo `adb shell pm uninstall --user 0 cn.wps.xiaomi.abroad.lite`
echo `adb shell pm uninstall com.opera.browser.afin`
echo `adb shell pm uninstall --user 0 com.opera.browser.afin`
echo `adb shell pm uninstall com.miui.fmservice`
echo `adb shell pm uninstall --user 0 com.miui.fmservice`
echo `adb shell pm uninstall com.google.android.setupwizard`
echo `adb shell pm uninstall --user 0 com.google.android.setupwizard`
echo `adb shell pm uninstall com.google.android.apps.safetyhub`
echo `adb shell pm uninstall --user 0 com.google.android.apps.safetyhub`
echo `adb shell pm uninstall com.xiaomi.calendar`
echo `adb shell pm uninstall --user 0 com.xiaomi.calendar`
echo `adb shell pm uninstall com.miui.backup`
echo `adb shell pm uninstall --user 0 com.miui.backup`
echo `adb shell pm uninstall com.myntra.android`
echo `adb shell pm uninstall --user 0 com.myntra.android`
#notification tools
#echo `adb shell pm uninstall com.xiaomi.barrage`
#echo `adb shell pm uninstall --user 0 com.xiaomi.barrage`
echo `adb shell pm uninstall com.google.android.apps.subscriptions.red`
echo `adb shell pm uninstall --user 0 com.google.android.apps.subscriptions.red`
echo `adb shell pm uninstall com.google.android.apps.messaging`
echo `adb shell pm uninstall --user 0 com.google.android.apps.messaging`
echo `adb shell pm uninstall com.google.android.tts`
echo `adb shell pm uninstall --user 0 com.google.android.tts`
echo `adb shell pm uninstall com.google.android.apps.podcasts`
echo `adb shell pm uninstall --user 0 com.google.android.apps.podcasts`
echo `adb shell pm uninstall com.android.mms`
echo `adb shell pm uninstall --user 0 com.android.mms`
echo `adb shell pm uninstall com.miui.miinput`
echo `adb shell pm uninstall --user 0 com.miui.miinput`
echo `adb shell pm uninstall com.miui.cloudservice`
echo `adb shell pm uninstall --user 0 com.miui.cloudservice`
echo `adb shell pm uninstall com.milink.service`
echo `adb shell pm uninstall --user 0 com.milink.service`
#echo `adb shell pm uninstall com.miui.misound`
#echo `adb shell pm uninstall --user 0 com.miui.misound`
echo `adb shell pm uninstall com.funnypuri.client`
echo `adb shell pm uninstall --user 0 com.funnypuri.client`
echo `adb shell pm uninstall com.facebook.system`
echo `adb shell pm uninstall --user 0 com.facebook.system`
echo `adb shell pm uninstall com.google.mainline.telemetry`
echo `adb shell pm uninstall --user 0 com.google.mainline.telemetry`
echo `adb shell pm uninstall com.facebook.katana`
echo `adb shell pm uninstall --user 0 com.facebook.katana`
echo `adb shell pm uninstall org.mipay.android.manager`
echo `adb shell pm uninstall --user 0 org.mipay.android.manager`
echo `adb shell pm uninstall com.tencent.soter.soterserver`
echo `adb shell pm uninstall --user 0 com.tencent.soter.soterserver`
echo `adb shell pm uninstall com.xiaomi.mi_care`
echo `adb shell pm uninstall --user 0 com.xiaomi.mi_care`
echo `adb shell pm uninstall com.google.android.youtube`
echo `adb shell pm uninstall --user 0 com.google.android.youtube`
